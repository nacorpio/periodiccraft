package com.periodiccraft.pcm;

import net.minecraft.block.Block;
import net.minecraft.init.Items;

import com.periodiccraft.pcm.core.block.BlockOreAluminium;
import com.periodiccraft.pcm.core.block.BlockOreIridium;
import com.periodiccraft.pcm.core.block.BlockOrePoor;
import com.periodiccraft.pcm.core.block.BlockOreRich;
import com.periodiccraft.pcm.core.block.BlockOreTin;
import com.periodiccraft.pcm.core.block.BlockOreUranium;
import com.periodiccraft.pcm.core.block.PeriodicBlockKnowledge;
import com.periodiccraft.pcm.core.element.Assemblies;
import com.periodiccraft.pcm.core.element.Elements;
import com.periodiccraft.pcm.core.element.PhysicalAssemblies;
import com.periodiccraft.pcm.core.element.PhysicalAssembly;
import com.periodiccraft.pcm.core.element.part.ElementAluminium;
import com.periodiccraft.pcm.core.element.part.ElementAntimony;
import com.periodiccraft.pcm.core.element.part.ElementArgon;
import com.periodiccraft.pcm.core.element.part.ElementArsenic;
import com.periodiccraft.pcm.core.element.part.ElementBeryllium;
import com.periodiccraft.pcm.core.element.part.ElementBoron;
import com.periodiccraft.pcm.core.element.part.ElementBromine;
import com.periodiccraft.pcm.core.element.part.ElementCadmium;
import com.periodiccraft.pcm.core.element.part.ElementCalcium;
import com.periodiccraft.pcm.core.element.part.ElementCarbon;
import com.periodiccraft.pcm.core.element.part.ElementChlorine;
import com.periodiccraft.pcm.core.element.part.ElementChromium;
import com.periodiccraft.pcm.core.element.part.ElementCobalt;
import com.periodiccraft.pcm.core.element.part.ElementCopper;
import com.periodiccraft.pcm.core.element.part.ElementFluorine;
import com.periodiccraft.pcm.core.element.part.ElementGallium;
import com.periodiccraft.pcm.core.element.part.ElementGermanium;
import com.periodiccraft.pcm.core.element.part.ElementGold;
import com.periodiccraft.pcm.core.element.part.ElementHelium;
import com.periodiccraft.pcm.core.element.part.ElementHydrogen;
import com.periodiccraft.pcm.core.element.part.ElementIndium;
import com.periodiccraft.pcm.core.element.part.ElementIodine;
import com.periodiccraft.pcm.core.element.part.ElementIron;
import com.periodiccraft.pcm.core.element.part.ElementKrypton;
import com.periodiccraft.pcm.core.element.part.ElementLithium;
import com.periodiccraft.pcm.core.element.part.ElementMagnesium;
import com.periodiccraft.pcm.core.element.part.ElementManganese;
import com.periodiccraft.pcm.core.element.part.ElementMolybdenum;
import com.periodiccraft.pcm.core.element.part.ElementNeon;
import com.periodiccraft.pcm.core.element.part.ElementNickel;
import com.periodiccraft.pcm.core.element.part.ElementNiobium;
import com.periodiccraft.pcm.core.element.part.ElementNitrogen;
import com.periodiccraft.pcm.core.element.part.ElementOxygen;
import com.periodiccraft.pcm.core.element.part.ElementPalladium;
import com.periodiccraft.pcm.core.element.part.ElementPhosphorus;
import com.periodiccraft.pcm.core.element.part.ElementPotassium;
import com.periodiccraft.pcm.core.element.part.ElementRhodium;
import com.periodiccraft.pcm.core.element.part.ElementRubidium;
import com.periodiccraft.pcm.core.element.part.ElementRuthenium;
import com.periodiccraft.pcm.core.element.part.ElementScandium;
import com.periodiccraft.pcm.core.element.part.ElementSelenium;
import com.periodiccraft.pcm.core.element.part.ElementSilicon;
import com.periodiccraft.pcm.core.element.part.ElementSilver;
import com.periodiccraft.pcm.core.element.part.ElementSodium;
import com.periodiccraft.pcm.core.element.part.ElementStrontium;
import com.periodiccraft.pcm.core.element.part.ElementSulfur;
import com.periodiccraft.pcm.core.element.part.ElementTechnetium;
import com.periodiccraft.pcm.core.element.part.ElementTellurium;
import com.periodiccraft.pcm.core.element.part.ElementTitanium;
import com.periodiccraft.pcm.core.element.part.ElementVanadium;
import com.periodiccraft.pcm.core.element.part.ElementXenon;
import com.periodiccraft.pcm.core.element.part.ElementYttrium;
import com.periodiccraft.pcm.core.element.part.ElementZinc;
import com.periodiccraft.pcm.core.element.part.ElementZirconium;
import com.periodiccraft.pcm.core.knowledge.BlockKnowledge;
import com.periodiccraft.pcm.core.knowledge.ItemKnowledge;
import com.periodiccraft.pcm.core.knowledge.KnowledgeBase;
import com.periodiccraft.pcm.core.world.generation.OreWorldGenerator;
import com.periodiccraft.pcm.core.world.generation.PoorWorldGenerator;
import com.periodiccraft.pcm.core.world.generation.RichWorldGenerator;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = PeriodicCraft.MODID, version = PeriodicCraft.VERSION)
public class PeriodicCraft {
	
    public static final String MODID = "pcm";
    public static final String VERSION = "0.0.1";
    
    public static Block blockKnowledge;
    
    public static Block blockOreAluminium;
    public static Block blockOreIridium;
    public static Block blockOreTin;
    public static Block blockOreUranium;
    
    public static Block blockOrePoor;
    public static Block blockOreRich;
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
		
    	this.initElements();
    	this.initAssemblies();
    	this.initKnowledge();
    	
    }
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	
    	blockKnowledge = new PeriodicBlockKnowledge().setBlockName("blockKnowledge").setHardness(6.0F).setResistance(25.0F);  
    	blockOreIridium = new BlockOreIridium().setBlockName("blockOreIridium").setHardness(3.0F).setResistance(15.0F);
    	blockOreTin = new BlockOreTin().setBlockName("blockOreTin").setHardness(3.0F).setResistance(15.0F);
    	blockOreUranium = new BlockOreUranium().setBlockName("blockOreUranium").setHardness(3.0F).setResistance(15.0F);
    	blockOreAluminium = new BlockOreAluminium().setBlockName("blockOreAluminium").setHardness(3.0F).setResistance(15.0F);
    	
    	blockOrePoor = new BlockOrePoor().setBlockName("blockOrePoor").setHardness(3.0F).setResistance(15.0F);
    	blockOreRich = new BlockOreRich().setBlockName("blockOreRich").setHardness(6.0F).setResistance(20.0F);
    	
		GameRegistry.registerBlock(blockKnowledge, blockKnowledge.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockOreIridium, blockOreIridium.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockOreTin, blockOreTin.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockOreUranium, blockOreUranium.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockOreAluminium, blockOreAluminium.getUnlocalizedName().substring(5));
		
		GameRegistry.registerBlock(blockOrePoor, blockOrePoor.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(blockOreRich, blockOreRich.getUnlocalizedName().substring(5));
		
		GameRegistry.registerWorldGenerator(new OreWorldGenerator(), 1);
		GameRegistry.registerWorldGenerator(new PoorWorldGenerator(), 2);
		GameRegistry.registerWorldGenerator(new RichWorldGenerator(), 3);
		
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	
    }
    
    public final void initKnowledge() {
    	
    	KnowledgeBase.addKnowledge(blockOreIridium.getUnlocalizedName(), new BlockKnowledge(blockOreIridium));
    	KnowledgeBase.addKnowledge(blockOreUranium.getUnlocalizedName(), new BlockKnowledge(blockOreUranium));
    	KnowledgeBase.addKnowledge(blockOreTin.getUnlocalizedName(), new BlockKnowledge(blockOreTin));
    	KnowledgeBase.addKnowledge(blockOreAluminium.getUnlocalizedName(), new BlockKnowledge(blockOreAluminium));
    	
    	KnowledgeBase.addKnowledge(Items.paper.getUnlocalizedName(), new ItemKnowledge(Items.paper));
    	KnowledgeBase.addKnowledge(Items.iron_ingot.getUnlocalizedName(), new ItemKnowledge(Items.iron_ingot));
    	KnowledgeBase.addKnowledge(Items.gold_ingot.getUnlocalizedName(), new ItemKnowledge(Items.gold_ingot));
    	KnowledgeBase.addKnowledge(Items.diamond.getUnlocalizedName(), new ItemKnowledge(Items.diamond));
    	KnowledgeBase.addKnowledge(Items.bucket.getUnlocalizedName(), new ItemKnowledge(Items.bucket));
    	KnowledgeBase.addKnowledge(Items.coal.getUnlocalizedName(), new ItemKnowledge(Items.coal));
    	KnowledgeBase.addKnowledge(Items.string.getUnlocalizedName(), new ItemKnowledge(Items.string));
    	
    }
    
    public final void initAssemblies() {
    	
    	Assemblies.addAssembly(Items.iron_ingot.getUnlocalizedName(), Assemblies.IRON_INGOT);
    	Assemblies.addAssembly(Items.gold_ingot.getUnlocalizedName(), Assemblies.GOLD_INGOT);
    	Assemblies.addAssembly(Items.diamond.getUnlocalizedName(), Assemblies.DIAMOND);
    	Assemblies.addAssembly(Items.bucket.getUnlocalizedName(), Assemblies.WATER);
    	Assemblies.addAssembly(Items.coal.getUnlocalizedName(), Assemblies.COAL);
    	
    	// Physical Assemblies Initialization
    	PhysicalAssemblies.addAssembly(Items.gold_ingot.getUnlocalizedName(), new PhysicalAssembly(1000F, Items.gold_ingot.getUnlocalizedName(), new ElementGold(8)));
    	PhysicalAssemblies.addAssembly(Items.iron_ingot.getUnlocalizedName(), new PhysicalAssembly(1000F, Items.iron_ingot.getUnlocalizedName(), new ElementIron(8)));
    	PhysicalAssemblies.addAssembly(Items.diamond.getUnlocalizedName(), new PhysicalAssembly(750F, Items.diamond.getUnlocalizedName(), new ElementCarbon(64)));
    	PhysicalAssemblies.addAssembly(Items.coal.getUnlocalizedName(), new PhysicalAssembly(150F, Items.coal.getUnlocalizedName(), new ElementCarbon(8)));
    	PhysicalAssemblies.addAssembly(Items.paper.getUnlocalizedName(), new PhysicalAssembly(25F, Items.paper.getUnlocalizedName(),
    									new ElementCarbon(6),
    									new ElementHydrogen(10),
    									new ElementOxygen(5)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.string.getUnlocalizedName(), new PhysicalAssembly(25F, Items.string.getUnlocalizedName(),
    									new ElementCarbon(12),
    									new ElementHydrogen(29),
    									new ElementNitrogen(5),
    									new ElementOxygen(11)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.gunpowder.getUnlocalizedName(), new PhysicalAssembly(150F, Items.gunpowder.getUnlocalizedName(),
    									new ElementPotassium(10),
    									new ElementNitrogen(10),
    									new ElementOxygen(30),
    									new ElementSulfur(3),
    									new ElementCarbon(8)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.apple.getUnlocalizedName(), new PhysicalAssembly(200F, Items.apple.getUnlocalizedName(),
						    			new ElementHydrogen(6),
										new ElementOxygen(5),
										new ElementCarbon(4) 
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.golden_apple.getUnlocalizedName(), new PhysicalAssembly(800F, Items.golden_apple.getUnlocalizedName(), 
    									new ElementHydrogen(6),
    									new ElementOxygen(5),
    									new ElementCarbon(4),
    									new ElementGold(3)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.clay_ball.getUnlocalizedName(), new PhysicalAssembly(1000F, Items.clay_ball.getUnlocalizedName(),
    									new ElementAluminium(3),
    									new ElementSilicon(3),
    									new ElementOxygen(4)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.bone.getUnlocalizedName(), new PhysicalAssembly(400F, Items.bone.getUnlocalizedName(),
    									new ElementCalcium(10),
    									new ElementPhosphorus(6),
    									new ElementOxygen(26),
    									new ElementHydrogen(2)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.sugar.getUnlocalizedName(), new PhysicalAssembly(100F, Items.sugar.getUnlocalizedName(), 
    									new ElementCarbon(6),
    									new ElementHydrogen(12),
    									new ElementOxygen(6)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.emerald.getUnlocalizedName(), new PhysicalAssembly(1000F, Items.emerald.getUnlocalizedName(),
    									new ElementBeryllium(3),
    									new ElementAluminium(2),
    									new ElementSilicon(6),
    									new ElementOxygen(18)
    	));
    	
    	PhysicalAssemblies.addAssembly(Items.flint.getUnlocalizedName(), new PhysicalAssembly(100F, Items.flint.getUnlocalizedName(),
    									new ElementSilicon(1),
    									new ElementOxygen(2)
    	));
    	
    	
    	PhysicalAssemblies.addAssembly(Items.bucket.getUnlocalizedName(), new PhysicalAssembly(1500F, Items.bucket.getUnlocalizedName(),
    									new ElementIron(24)
    			
    	));
    	
    }
    
    public final void initElements() {
    	
    	// Elements Initialization
    	Elements.addElement("C", new ElementCarbon(1));
    	Elements.addElement("H", new ElementHydrogen(1));
    	Elements.addElement("N", new ElementNitrogen(1));
    	Elements.addElement("O", new ElementOxygen(1));
    	Elements.addElement("Au", new ElementGold(1));
    	Elements.addElement("Fe", new ElementIron(1));
    	Elements.addElement("Be", new ElementBeryllium(1));
    	Elements.addElement("Al", new ElementAluminium(1));
    	Elements.addElement("Si", new ElementSilicon(1));
    	Elements.addElement("He", new ElementHelium(1));
    	Elements.addElement("Li", new ElementLithium(1));
    	Elements.addElement("B", new ElementBoron(1));
    	Elements.addElement("F", new ElementFluorine(1));
    	Elements.addElement("Ne", new ElementNeon(1));
    	Elements.addElement("Na", new ElementSodium(1));
    	Elements.addElement("Mg", new ElementMagnesium(1));
    	Elements.addElement("P", new ElementPhosphorus(1));
    	Elements.addElement("S", new ElementSulfur(1));
    	Elements.addElement("Cl", new ElementChlorine(1));
    	Elements.addElement("Ar", new ElementArgon(1));
    	Elements.addElement("Zn", new ElementZinc(1));
    	Elements.addElement("Cu", new ElementCopper(1));
    	Elements.addElement("Ni", new ElementNickel(1));
    	Elements.addElement("Ga", new ElementGallium(1));
    	Elements.addElement("Ti", new ElementTitanium(1));
    	Elements.addElement("Co", new ElementCobalt(1));
    	Elements.addElement("Ge", new ElementGermanium(1));
    	Elements.addElement("Sc", new ElementScandium(1));
    	Elements.addElement("Cr", new ElementChromium(1));
    	Elements.addElement("Mn", new ElementManganese(1));
    	Elements.addElement("V", new ElementVanadium(1));
    	Elements.addElement("As", new ElementArsenic(1));
    	Elements.addElement("Br", new ElementBromine(1));
    	Elements.addElement("Kr", new ElementKrypton(1));
    	Elements.addElement("Ca", new ElementCalcium(1));
    	Elements.addElement("Se", new ElementSelenium(1));
    	Elements.addElement("K", new ElementPotassium(1));
    	Elements.addElement("Rb", new ElementRubidium(1));
    	Elements.addElement("Sr", new ElementStrontium(1));
    	Elements.addElement("Y", new ElementYttrium(1));
    	Elements.addElement("Zr", new ElementZirconium(1));
    	Elements.addElement("Nb", new ElementNiobium(1));
    	Elements.addElement("Mo", new ElementMolybdenum(1));
    	Elements.addElement("Tc", new ElementTechnetium(1));
    	Elements.addElement("Ru", new ElementRuthenium(1));
    	Elements.addElement("Rh", new ElementRhodium(1));
    	Elements.addElement("Pd", new ElementPalladium(1));
    	Elements.addElement("Ag", new ElementSilver(1));
    	Elements.addElement("Cd", new ElementCadmium(1));
    	Elements.addElement("In", new ElementIndium(1));
    	Elements.addElement("Sn", new ElementSilver(1));
    	Elements.addElement("Sb", new ElementAntimony(1));
    	Elements.addElement("Te", new ElementTellurium(1));
    	Elements.addElement("I", new ElementIodine(1));
    	Elements.addElement("Xe", new ElementXenon(1));
    	
    }
    
}
