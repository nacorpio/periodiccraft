package com.periodiccraft.pcm.core.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import com.periodiccraft.pcm.PeriodicCraft;
import com.periodiccraft.pcm.api.util.ChatUtil;
import com.periodiccraft.pcm.core.knowledge.ItemKnowledge;
import com.periodiccraft.pcm.core.knowledge.KnowledgeBase;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class PeriodicBlockKnowledge extends Block {

	@SideOnly(Side.CLIENT)
	public static IIcon sideIcon;
	
	@SideOnly(Side.CLIENT)
	public static IIcon bottomIcon;
	
	@SideOnly(Side.CLIENT)
	public static IIcon topIcon;
	
	//@SideOnly(Side.CLIENT)
	//public static Icon frontIcon;
	
	public PeriodicBlockKnowledge() {
		super(Material.rock);
		this.setCreativeTab(CreativeTabs.tabBlock);
	}

	public final boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
		if (par1World.isRemote) {
			if (par5EntityPlayer.inventory.getCurrentItem() != null) {
				ItemStack stack = par5EntityPlayer.inventory.getCurrentItem();
				if (KnowledgeBase.hasItemKnowledge(par5EntityPlayer.inventory.getCurrentItem().getUnlocalizedName())) {
					ItemKnowledge knowledge = KnowledgeBase.getItemKnowledge(par5EntityPlayer.inventory.getCurrentItem());
					ChatUtil.sendChatMessage("Knowledge for (" + stack.getDisplayName() + "): " + ChatUtil.StringHandler.green + (knowledge.isCompleted() ? "Completed" : knowledge.getProgress() + "/100"));
					return true;
				} else {
					ChatUtil.sendChatMessage(ChatUtil.StringHandler.red + "Knowledge is not available for this item.");
				}
			}
		}	
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public final void registerBlockIcons(IIconRegister par1IconRegister) {
		sideIcon = par1IconRegister.registerIcon(PeriodicCraft.MODID + ":" + this.getUnlocalizedName().substring(5) + "_side");
		bottomIcon = par1IconRegister.registerIcon(PeriodicCraft.MODID + ":" + this.getUnlocalizedName().substring(5) + "_bottom");
		topIcon = par1IconRegister.registerIcon(PeriodicCraft.MODID + ":" + this.getUnlocalizedName().substring(5) + "_top");
		// frontIcon = par1IconRegister.registerIcon("blockKnowledge_front");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
		switch (side) {
		case 0:
			return bottomIcon;
		case 1:
			return topIcon;
		default:
			return sideIcon;
		}
	}
	
	@Override
	public int quantityDropped(Random random) {
		return 1;
	}
	
}
