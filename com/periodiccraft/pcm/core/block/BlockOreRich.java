package com.periodiccraft.pcm.core.block;

import java.util.Random;

import com.periodiccraft.pcm.PeriodicCraft;

import net.minecraft.block.BlockOre;
import net.minecraft.creativetab.CreativeTabs;

public class BlockOreRich extends PeriodicOre {

	public BlockOreRich() {
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setBlockTextureName(PeriodicCraft.MODID + ":blockOreRich");
	}

	@Override
	public int quantityDropped(Random random) {
		return 1;
	}
	
}
