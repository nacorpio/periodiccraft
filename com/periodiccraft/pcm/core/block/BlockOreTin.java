package com.periodiccraft.pcm.core.block;

import java.util.Random;

import com.periodiccraft.pcm.PeriodicCraft;

import net.minecraft.block.BlockOre;
import net.minecraft.creativetab.CreativeTabs;

public class BlockOreTin extends PeriodicOre {

	public BlockOreTin() {
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setBlockTextureName(PeriodicCraft.MODID + ":blockOreTin");
	}

	@Override
	public int quantityDropped(Random random) {
		return 1;
	}
	
}
