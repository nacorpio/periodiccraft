package com.periodiccraft.pcm.core.block;

import net.minecraft.block.BlockOre;
import net.minecraft.creativetab.CreativeTabs;

public class PeriodicOre extends BlockOre {

	public PeriodicOre() {
		this.setCreativeTab(CreativeTabs.tabBlock);
	}
	
}
