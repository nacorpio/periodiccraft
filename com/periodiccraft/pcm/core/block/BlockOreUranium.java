package com.periodiccraft.pcm.core.block;

import java.util.Random;

import com.periodiccraft.pcm.PeriodicCraft;

import net.minecraft.block.BlockOre;
import net.minecraft.creativetab.CreativeTabs;

public class BlockOreUranium extends PeriodicOre {

	public BlockOreUranium() {
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setBlockTextureName(PeriodicCraft.MODID + ":blockOreUranium");
	}

	@Override
	public int quantityDropped(Random random) {
		return 1;
	}
	
}
