package com.periodiccraft.pcm.core.block;

import java.util.Random;

import com.periodiccraft.pcm.PeriodicCraft;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockOre;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockOrePoor extends Block {

	public BlockOrePoor() {
		super(Material.rock);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setBlockTextureName(PeriodicCraft.MODID + ":blockOrePoor");
	}

	@Override
	public int quantityDropped(Random random) {
		return 1;
	}

}