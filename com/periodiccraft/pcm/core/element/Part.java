package com.periodiccraft.pcm.core.element;

import net.minecraft.item.Item;

public class Part {

	private String name;
	private String symbol;
	private float mol;
	private int count;
	
	private ElementState state;
	
	public Part(String par1, String par2, float par4) {
		this(par1, par2, ElementState.SOLID, par4);
	}
	
	public Part(String par1, String par2, ElementState par3, float par4) {
		this(par1, par2, par3, par4, 1);
	}
	
	public Part(String par1, String par2, float par3, int par4) {
		this(par1, par2, ElementState.SOLID, par3, par4);
	}
	
	public Part(String par1, String par2, ElementState par3, float par4, int par5) {
		symbol = par1;
		name = par2;
		state = par3;
		mol = par4;
		count = par5;
	}

	/**
	 * Shortens the time and the space it takes to add a part in an array.
	 * @param par1 the name of the Item.
	 * @param par2 the count of parts.
	 * @return the Part.
	 */
	public static final Part b(String par1, String par2, ElementState par3, int par4) {
		return new Part(par1, par2, par3, par4);
	}
	
	/**
	 * Returns the name of the Part.
	 * @return the name.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Set a new count.
	 * @param par1 the amount of building-parts.
	 * @return the Part for convenience purposes.
	 */
	public final Part setCount(int par1) {
		this.count = par1;
		return this;
	}
	
	public final void setState(ElementState par1) {
		this.state = par1;
	}
	
	public final ElementState getState() {
		return state;
	}
	
	public final String getSymbol() {
		return symbol;
	}
	
	public final float getAtomicMass() {
		return mol;
	}
	
	public final float getTotalMass() {
		return mol * count;
	}
	
	/**
	 * Returns the amount of parts.
	 * @return the amount.
	 */
	public final int getCount() {
		return count;
	}
	
}
