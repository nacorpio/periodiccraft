package com.periodiccraft.pcm.core.element;

import java.util.HashMap;
import java.util.Map;

public final class Elements {

	public static Map<String, Part> elements = new HashMap<String, Part>();
	
	public static final void addElement(String par1, Part par2) {
		if (!hasElement(par1)) {
			elements.put(par1, par2);
		}
	}
	
	public static final Part getElement(String par1) {
		if (hasElement(par1)) {
			return elements.get(par1);
		}
		return null;
	}
	
	public static final boolean hasElement(String par1) {
		return elements.containsKey(par1);
	}
	
}
