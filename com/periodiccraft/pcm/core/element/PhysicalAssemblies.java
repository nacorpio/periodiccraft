package com.periodiccraft.pcm.core.element;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.Item;

public class PhysicalAssemblies {

	public static final Map<String, PhysicalAssembly> assemblies = new HashMap<String, PhysicalAssembly>();
	
	public static final void addAssembly(String par1, PhysicalAssembly par2) {
		if (!hasAssembly(par1)) {
			assemblies.put(par1, par2);
		}
	}
	
	public static final PhysicalAssembly getAssembly(String par1) {
		if (hasAssembly(par1)) {
			return assemblies.get(par1);
		}
		return null;
	}
	
	public static final boolean hasAssembly(String par1) {
		return assemblies.containsKey(par1);
	}
	
}
