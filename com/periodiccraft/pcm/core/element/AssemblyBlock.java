package com.periodiccraft.pcm.core.element;

public class AssemblyBlock {

	private String blockName;
	private Part[] parts;
	
	public AssemblyBlock(String par1, Part... par2) {
		this.blockName = par1;
		this.parts = par2;
	}
	
	public final ElementStack getElementStack() {
		return new ElementStack(parts);
	}
	
	public final String getBlockName() {
		return blockName;
	}
	
	public final Part[] getParts() {
		return parts;
	}
	
}
