package com.periodiccraft.pcm.core.element;

import net.minecraft.item.Item;

/**
 * This is an assembly that can just be viewed, not modified.<br>
 * To modify an assembly, use the {@link #MAssembly}.
 */
public class Assembly {

	private String itemName;
	private Part[] parts;
	
	/**
	 * Creates a new Assembly.
	 * @param par1 the itemId of the assembled Item.
	 * @param par2 the parts returned when disassembling this item.
	 */
	public Assembly(String par1, Part... par2) {
		this.itemName = par1;
		this.parts = par2;
	}
	
	/**
	 * Returns the ItemID of the Item that is assembled through the building-parts.
	 * @return the ItemID.
	 */
	public final String getItemName() {
		return itemName;
	}
	
	/**
	 * Returns the formula of the Assembly/Compound Element.<br>
	 * This formula might not always be a 100% correct.
	 * @return the formula.
	 */
	public final String getFormula() {
		String formula = "";
		for (Part var: parts) {
			if (var.getCount() > 1) {
				formula += var.getSymbol() + var.getCount();
			} else {
				formula += var.getSymbol();
			}
		}
		return formula;
	}
	
	/**
	 * Returns an ElementStack containing extra methods for interacting with the contents.<br>
	 * This will also give you methods to get the molarmass and similar.
	 * @return the ElementStack.
	 */
	public final ElementStack getElementStack() {
		return new ElementStack(parts);
	}
	
	/**
	 * Returns the building-parts needed to assemble an Item of the {@link #getItemId()}.
	 * @return the building-parts.
	 */
	public final Part[] getParts() {
		return parts;
	}
	
}
