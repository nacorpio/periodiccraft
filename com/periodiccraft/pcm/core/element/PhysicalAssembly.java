package com.periodiccraft.pcm.core.element;

public class PhysicalAssembly extends Assembly {
	
	float weightGrams;
	
	public PhysicalAssembly(float par1, String par2, Part... par3) {
		super(par2, par3);
		this.weightGrams = par1;
	}
	
	public final float getWeightGrams() {
		return weightGrams;
	}
	
	public final float getMoles() {
		return this.getElementStack().getMass() / weightGrams;
	}
	
}
