package com.periodiccraft.pcm.core.element;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.init.Items;

import com.periodiccraft.pcm.core.element.part.ElementCarbon;
import com.periodiccraft.pcm.core.element.part.ElementGold;
import com.periodiccraft.pcm.core.element.part.ElementHydrogen;
import com.periodiccraft.pcm.core.element.part.ElementIron;
import com.periodiccraft.pcm.core.element.part.ElementOxygen;

public final class Assemblies {
	
	// In-game elements.
	public final static Assembly STICK = new Assembly(Items.stick.getUnlocalizedName(), new ElementCarbon(1));
	
	// Ingots
	public final static Assembly IRON_INGOT = new Assembly(Items.iron_ingot.getUnlocalizedName(), new ElementIron(1));
	public final static Assembly GOLD_INGOT = new Assembly(Items.gold_ingot.getUnlocalizedName(), new ElementGold(1));
	public final static Assembly COAL = new Assembly(Items.coal.getUnlocalizedName(), new ElementCarbon(1));
	public final static Assembly DIAMOND = new Assembly(Items.diamond.getUnlocalizedName(), new ElementCarbon(1));
	public final static Assembly WATER = new Assembly(Items.water_bucket.getUnlocalizedName(), new ElementHydrogen(2), new ElementOxygen(1), new ElementIron(1));
	
	public static final Map<String, Assembly> assemblies = new HashMap<String, Assembly>();
	
	/**
	 * Add a new assembly to the Assembly Dictionary.
	 * @param par1 the itemId of the {@link #Item} using the assembly.
	 * @param par2 the assembly to associate with with the {@link #Item}.
	 */
	public static final void addAssembly(String par1, Assembly par2) {
		if (!hasAssembly(par1)) {
			assemblies.put(par1, par2);
		}
	}
	
	/**
	 * Returns the assembly associated to the specified itemId.
	 * @param par1 the itemId to return the assembly for.
	 * @return the associated assembly.
	 */
	public static final Assembly getAssembly(String par1) {
		if (hasAssembly(par1)) {
			return assemblies.get(par1);
		}
		return null;
	}
	
	/**
	 * Returns whether the dictionary stores an assembly holding data for the specified itemId.
	 * @param par1 the itemId to check the existence of.
	 * @return whether the field exists.
	 */
	public static final boolean hasAssembly(String par1) {
		return assemblies.containsKey(par1);
	}
	
}
