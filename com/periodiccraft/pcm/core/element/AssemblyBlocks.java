package com.periodiccraft.pcm.core.element;

import java.util.HashMap;
import java.util.Map;

public final class AssemblyBlocks {

	public static final Map<String, AssemblyBlock> blocks = new HashMap<String, AssemblyBlock>();
	
	public static final void addAssembly(String par1, AssemblyBlock par2) {
		if (!hasAssembly(par1)) {
			blocks.put(par1, par2);
		}
	}
	
	public static final AssemblyBlock getAssembly(String par1) {
		if (hasAssembly(par1)) {
			return blocks.get(par1);
		}
		return null;
	}
	
	public static final boolean hasAssembly(String par1) {
		return blocks.containsKey(par1);
	}
	
}
