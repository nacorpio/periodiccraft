package com.periodiccraft.pcm.core.element.recipe;

import com.periodiccraft.pcm.core.element.Assembly;
import com.periodiccraft.pcm.core.element.Part;

public class ElementRecipe {

	private Part e1;
	private Part e2;
	
	private RecipeType type;
	private Assembly product;
	
	public ElementRecipe(Part par1, Part par2, Assembly par3, RecipeType par4) {
		this.e1 = par1;
		this.e2 = par2;
		this.product = par3;
		this.type = par4;
	}
	
	public final Part getPart1() {
		return e1;
	}
	
	public final Part getPart2() {
		return e2;
	}
	
	public final Assembly getProduct() {
		return product;
	}
	
	public final RecipeType getRecipeType() {
		return type;
	}
	
}
