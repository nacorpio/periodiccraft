package com.periodiccraft.pcm.core.element.recipe;

public enum RecipeType {

	MOLECULAR_ASSEMBLER,
	MOLECULAR_CRAFTING_TABLE,
	ELEMENT_REACTION,
	HEAT,
	ENERGY,
	PRESSURE;
	
}
