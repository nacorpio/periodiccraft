package com.periodiccraft.pcm.core.element.recipe;

import java.util.HashMap;
import java.util.Map;

import com.periodiccraft.pcm.core.element.Assembly;
import com.periodiccraft.pcm.core.element.Part;

public final class RecipeManager {

	public static final Map<String, ElementRecipe> recipes = new HashMap<String, ElementRecipe>();
	
	/**
	 * Get a product out of the parts/elements that you add.
	 * @param par1 the type of reaction you create with the elements.
	 * @param par2 the parts/elements to create a reaction with.
	 * @return the product.
	 */
	public static final Assembly getProduct(String par1, RecipeType par2) {
		ElementRecipe var1 = getRecipe(par1);
		if (var1.getRecipeType().equals(par2)) {
			return var1.getProduct();
		}
		return null;
	}
	
	/**
	 * Get the recipe for the item with the itemId you specify.
	 * @param par1 the itemId of the target Item.
	 * @return the recipe of the Item with that itemId.
	 */
	public static final ElementRecipe getRecipeFor(String par1) {
		for (ElementRecipe var: recipes.values()) {
			if (var.getProduct().getItemName() == par1) {
				return var;
			}
		}
		return null;
	}
	
	public static final void addRecipe(String par1, ElementRecipe par2) {
		if (!hasRecipe(par1)) {
			recipes.put(par1, par2);
		}
	}
	
	public static final ElementRecipe getRecipe(String par1) {
		if (hasRecipe(par1)) {
			return recipes.get(par1);
		}
		return null;
	}
	
	public static final boolean hasRecipe(String par1) {
		return recipes.containsKey(par1);
	}
	
}
