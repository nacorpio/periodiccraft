package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementYttrium extends Part {

	public ElementYttrium(int par1) {
		super("Y", "Yttrium", ElementState.SOLID, 88.91F, par1);
	
	}

}
