package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementSilicon extends Part {

	public ElementSilicon(int par1) {
		super("Si", "Silicon", 28.0855F, par1);
	}

}
