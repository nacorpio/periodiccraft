package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementBeryllium extends Part {

	public ElementBeryllium(int par1) {
		super("Be", "Beryllium", 9.012182F, par1);
	}

}
