package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;

public class ElementAntimony extends Part {

	public ElementAntimony(int par1) {
		super("Sb", "Antimony", ElementState.SOLID, 121.7F, par1);
	}

}
