package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementOxygen extends Part {

	public ElementOxygen(int par1) {
		super("O", "Oxygen", ElementState.GAS, 15.999F, par1);
	}

}
