package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;

public class ElementAluminium extends Part {

	public ElementAluminium(int par1) {
		super("Al", "Aluminium", ElementState.SOLID, 26.9815386F, par1);
	}

}
