package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementManganese extends Part {

	public ElementManganese(int par1) {
		super("Mn", "Manganese", 54.93F, par1);
	}

}
