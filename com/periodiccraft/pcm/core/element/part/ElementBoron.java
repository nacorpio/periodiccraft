package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementBoron extends Part {
	public ElementBoron(int par1) {
		super("B", "Boron", 10.81F, par1);
	}
}
