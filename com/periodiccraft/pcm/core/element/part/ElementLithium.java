package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementLithium extends Part { 

	public ElementLithium(int par1) {
	super("Li", "Lithium", 6.94F, par1);
}
}
