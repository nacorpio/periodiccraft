package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementSilver extends Part {

	public ElementSilver(int par1) {
		super("Ag", "Silver", ElementState.SOLID, 107.9F, par1);
	}

}
