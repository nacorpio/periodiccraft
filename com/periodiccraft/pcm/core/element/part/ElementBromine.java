package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementBromine extends Part {

	public ElementBromine(int par1) {
		super("Br", "Bromine", ElementState.LIQUID, 79.90F, par1);
	}

}
