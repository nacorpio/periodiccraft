package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementGallium extends Part {

	public ElementGallium(int par1) {
		super("Ga", "Gallium", ElementState.LIQUID, 69.72F, par1);
	}

}
