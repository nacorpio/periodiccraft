package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementNickel extends Part {

	public ElementNickel(int par1) {
		super("Ni", "Nickel", 58.69F, par1);
	}

}
