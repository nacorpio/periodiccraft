package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementPotassium extends Part {

	public ElementPotassium(int par1) {
		super("K", "Potassium", 39.10F, par1);
	}

}
