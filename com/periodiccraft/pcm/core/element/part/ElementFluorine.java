package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementFluorine extends Part {
	public ElementFluorine(int par1) {
		super("F", "Fluorine", ElementState.GAS, 19.00F, par1);
	}
}
