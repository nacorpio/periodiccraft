package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementSodium extends Part {

	public ElementSodium(int par1) {
		super("Na", "Sodium", 22.99F, par1);
	}

}
