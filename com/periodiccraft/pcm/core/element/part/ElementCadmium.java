package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementCadmium extends Part {

	public ElementCadmium(int par1) {
		super("Cd", "Damium", ElementState.SOLID, 112.4F, par1);
	}

}
