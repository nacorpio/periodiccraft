package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementChlorine extends Part {

	public ElementChlorine(int par1) {
		super("Cl", "Chlorine", ElementState.GAS, 35.45F, par1);
	}

}
