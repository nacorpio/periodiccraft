package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementVanadium extends Part {

	public ElementVanadium(int par1) {
		super("V", "Vanadium", 50.94F, par1);
	}

}
