package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementRhodium extends Part {

	public ElementRhodium(int par1) {
		super("Rh", "Rhodium", ElementState.SOLID, 102.9F, par1);
	}

}
