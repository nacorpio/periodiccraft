package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementPalladium extends Part {

	public ElementPalladium(int par1) {
		super("Pd", "Palladium", ElementState.SOLID, 106.4F, par1);
	}

}
