package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementNitrogen extends Part {

	public ElementNitrogen(int par1) {
	super("N", "Nitrogen", ElementState.GAS, 14.007F, par1);
	}

}
