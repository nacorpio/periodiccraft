package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementZinc extends Part {

	public ElementZinc(int par1) {
		super("Zn", "Zinc", 65.41F, par1);
	}

}
