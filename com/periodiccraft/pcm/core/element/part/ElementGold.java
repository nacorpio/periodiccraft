package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementGold extends Part {

	public ElementGold(int par1) {
		super("Au", "Gold", 196.97F, par1);
	}

}
