package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementXenon extends Part {

	public ElementXenon(int par1) {
		super("Xe", "Xenon", ElementState.GAS, 131.3F, par1);
	}

}
