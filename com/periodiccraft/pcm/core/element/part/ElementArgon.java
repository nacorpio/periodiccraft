package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementArgon extends Part {

	public ElementArgon(int par1) {
		super("Ar", "Argon", ElementState.GAS, 39.95F, par1);
	}

}
