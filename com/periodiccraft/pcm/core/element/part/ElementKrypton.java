package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementKrypton extends Part {

	public ElementKrypton(int par1) {
		super("Kr", "Krypton", ElementState.GAS, 83.80F, par1);
	}

}
