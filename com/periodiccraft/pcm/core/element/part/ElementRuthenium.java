package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementRuthenium extends Part {

	public ElementRuthenium(int par1) {
		super("Ru", "Ruthenium", ElementState.SOLID, 101.1F, par1);
		
		
	}

}
