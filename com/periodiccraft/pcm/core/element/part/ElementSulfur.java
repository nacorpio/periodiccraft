package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementSulfur extends Part {

	public ElementSulfur(int par1) {
		super("S", "Sulfur", 32.06F, par1);
	}

}
