package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementSelenium extends Part {

	public ElementSelenium(int par1) {
		super("Se", "Selenium", 78.96F, par1);
	}

}
