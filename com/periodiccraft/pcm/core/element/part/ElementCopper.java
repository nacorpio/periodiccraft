package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementCopper extends Part {

	public ElementCopper(int par1) {
		super("Cu", "Copper", 63.55F, par1);
	}

}
