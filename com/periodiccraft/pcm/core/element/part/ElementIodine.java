package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementIodine extends Part {

	public ElementIodine(int par1) {
		super("I", "Iodine", ElementState.SOLID, 126.9F, par1);
	}

}
