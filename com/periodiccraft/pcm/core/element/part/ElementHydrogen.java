package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementHydrogen extends Part {

	public ElementHydrogen(int par1) {
		super("H", "Hydrogen", ElementState.GAS, 1.008F, par1);
	}

}
