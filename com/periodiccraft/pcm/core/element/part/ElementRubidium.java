package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementRubidium extends Part {

	public ElementRubidium(int par1) {
		super("Rb", "Rubidium", ElementState.SOLID, 85.47F, par1);
	}

}
