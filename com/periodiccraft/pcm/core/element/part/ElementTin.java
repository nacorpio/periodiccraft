package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementTin extends Part {

	public ElementTin(int par1) {
		super("Sn", "Tin", ElementState.SOLID, 118.7F, par1);
	}

}
