package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementTitanium extends Part {

	public ElementTitanium(int par1) {
		super("Ti", "Titanium", 47.87F, par1);
	}

}
