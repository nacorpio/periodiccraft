package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementStrontium extends Part {

	public ElementStrontium(int par1) {
		super("Sr", "Strontium", ElementState.SOLID, 87.62F, par1);
	}

}
