package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementCarbon extends Part {

	public ElementCarbon(int par1) {
		super("C", "Carbon", 12.011F, par1);
	}

}
