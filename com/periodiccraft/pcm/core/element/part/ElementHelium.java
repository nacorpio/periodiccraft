package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementHelium extends Part {

	public ElementHelium(int par1) {
		super("He", "Helium", ElementState.GAS, 4.003F, par1);
	}

}
