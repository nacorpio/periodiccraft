package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementScandium extends Part {

	public ElementScandium(int par1) {
		super("Sc", "Scandium", 44.96F, par1);
	}

}
