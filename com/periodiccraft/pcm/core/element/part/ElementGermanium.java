package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementGermanium extends Part {

	public ElementGermanium(int par1) {
		super("Ge", "Germanium", 74.92F, par1);
	}

}
