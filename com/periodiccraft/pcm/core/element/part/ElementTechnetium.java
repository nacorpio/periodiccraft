package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementTechnetium extends Part {

	public ElementTechnetium(int par1) {
		super("Tc", "Technetium", ElementState.SOLID, 98F, par1);
	
	//is found in uranium (tiny bits)
	}
	
	

}
