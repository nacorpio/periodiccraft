package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementMolybdenum extends Part {

	public ElementMolybdenum(int par1) {
		super("Mo", "Molybdenum", ElementState.SOLID, 95.94F, par1);
	}

}
