package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementCalcium extends Part {

	public ElementCalcium(int par1) {
		super("Ca", "Calcium", 40.08F, par1);
	}

}
