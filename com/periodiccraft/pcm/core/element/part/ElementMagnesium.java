package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementMagnesium extends Part {

	public ElementMagnesium(int par1) {
		super("Mg", "Magnesium", 24.30F, par1);
	}

}
