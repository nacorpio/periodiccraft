package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementNeon extends Part {
	public ElementNeon(int par1) {
		super("Ne", "Neon,", ElementState.GAS, 20.18F, par1);
	}
}
