package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementIndium extends Part {

	public ElementIndium(int par1) {
		super("In", "Indium", ElementState.SOLID, 114.8F, par1);
	}

}
