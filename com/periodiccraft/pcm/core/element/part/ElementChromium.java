package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementChromium extends Part {

	public ElementChromium(int par1) {
		super("Cr", "Chromium", 52.00F, par1);
	}

}
