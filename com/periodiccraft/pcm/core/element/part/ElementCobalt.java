package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementCobalt extends Part {

	public ElementCobalt(int par1) {
		super("Co", "Cobalt", 58.93F, par1);
	}

}
