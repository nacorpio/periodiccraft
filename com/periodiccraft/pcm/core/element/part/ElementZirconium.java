package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementZirconium extends Part {

	public ElementZirconium(int par1) {
		super("Zr", "Zirconium", ElementState.SOLID, 91.22F, par1);
	}

}
