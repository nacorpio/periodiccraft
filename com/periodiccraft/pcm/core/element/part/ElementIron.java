package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementIron extends Part {

	public ElementIron(int par1) {
		super("Fe", "Iron", 55.845F, par1);
	}

}
