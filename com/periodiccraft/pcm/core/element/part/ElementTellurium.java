package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementTellurium extends Part {

	public ElementTellurium(int par1) {
		super("Te", "Tellurium", ElementState.SOLID, 127.6F, par1);
	}

}
