package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.ElementState;
import com.periodiccraft.pcm.core.element.Part;


public class ElementNiobium extends Part {

	public ElementNiobium(int par1) {
		super("Nb", "Niobium", ElementState.SOLID, 92.91F, par1);
	}

}
