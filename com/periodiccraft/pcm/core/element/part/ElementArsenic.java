package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementArsenic extends Part {

	public ElementArsenic(int par1) {
		super("As", "Arsenic", 74.92F, par1);
	}

}
