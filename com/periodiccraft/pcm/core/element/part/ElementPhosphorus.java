package com.periodiccraft.pcm.core.element.part;

import com.periodiccraft.pcm.core.element.Part;


public class ElementPhosphorus extends Part {

	public ElementPhosphorus(int par1) {
		super("P", "Phosphorus", 30.97F, par1);
	}

}
