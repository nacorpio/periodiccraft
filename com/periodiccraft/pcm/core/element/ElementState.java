package com.periodiccraft.pcm.core.element;

public enum ElementState {
	SOLID,
	LIQUID,
	GAS,
	PLASMA;
}
