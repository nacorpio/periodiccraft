package com.periodiccraft.pcm.core.element;

public class ElementStack {

	private Part[] elements;
	
	public ElementStack(Part... par1) {
		this.elements = par1;
	}

	public final boolean hasElementByName(String par1) {
		for (Part var: elements) {
			if (var.getName().equalsIgnoreCase(par1)) {
				return true;
			}
		}
		return false;
	}
	
	public final boolean hasElementBySymbol(String par1) {
		for (Part var: elements) {
			if (var.getSymbol().equalsIgnoreCase(par1)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns how many moles there is in x amount of grams of the assembly/compount element.
	 * @param par1 the amount in grams.
	 * @return the moles.
	 */
	public final float getMoles(float par1) {
		return getMass() / par1;
	}
	
	/**
	 * Returns the mass of this ElementStack.
	 * @return the mass.
	 */
	public final float getMass() {
		float var1 = 0;
		for (Part var: elements) {
			var1 += var.getTotalMass();
		}
		return var1;
	}
	
	public final Part[] getParts() {
		return elements;
	}
	
}
