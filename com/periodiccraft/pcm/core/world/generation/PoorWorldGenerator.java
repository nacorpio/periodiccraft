package com.periodiccraft.pcm.core.world.generation;

import java.util.Random;

import com.periodiccraft.pcm.PeriodicCraft;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class PoorWorldGenerator implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch (world.provider.dimensionId) {
		case -1:
			break;
		case 0:
			generateSurface(world, random, chunkX * 16, chunkZ * 16);
		}
	}
		
	private void generateSurface(World world, Random random, int BlockX, int BlockZ) {
		for(int i = 0; i < 7; i++){
			int Xcoord = BlockX + random.nextInt(16);
			int Zcoord = BlockZ + random.nextInt(16);
			int Ycoord = random.nextInt(64);	
			(new WorldGenMinable(PeriodicCraft.blockOrePoor, 4)).generate(world, random, Xcoord, Ycoord, Zcoord);
		}
	}

}