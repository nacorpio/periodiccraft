package com.periodiccraft.pcm.core.world.generation;

import java.util.Random;

import com.periodiccraft.pcm.PeriodicCraft;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class OreWorldGenerator implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch (world.provider.dimensionId) {
		case -1:
			break;
		case 0:
			generateSurfaceIridium(world, random, chunkX * 16, chunkZ * 16);
			generateSurfaceUranium(world, random, chunkX * 16, chunkZ * 16);
			generateSurfaceTin(world, random, chunkX * 16, chunkZ * 16);
			generateSurfaceAluminium(world, random, chunkX * 16, chunkZ * 16);
		}
	}

	private void generateSurfaceAluminium(World world, Random random, int BlockX, int BlockZ) {
		for(int i = 0; i < 12; i++){
			int Xcoord = BlockX + random.nextInt(16);
			int Zcoord = BlockZ + random.nextInt(16);
			int Ycoord = random.nextInt(64);	
			(new WorldGenMinable(PeriodicCraft.blockOreAluminium, 7)).generate(world, random, Xcoord, Ycoord, Zcoord);
		}
	}
	
	private void generateSurfaceTin(World world, Random random, int BlockX, int BlockZ) {
		for(int i = 0; i < 12; i++){
			int Xcoord = BlockX + random.nextInt(16);
			int Zcoord = BlockZ + random.nextInt(16);
			int Ycoord = random.nextInt(64);	
			(new WorldGenMinable(PeriodicCraft.blockOreTin, 7)).generate(world, random, Xcoord, Ycoord, Zcoord);
		}
	}
	
	private void generateSurfaceUranium(World world, Random random, int BlockX, int BlockZ) {
		for(int i = 0; i < 2; i++){
			int Xcoord = BlockX + random.nextInt(16);
			int Zcoord = BlockZ + random.nextInt(16);
			int Ycoord = random.nextInt(16);	
			(new WorldGenMinable(PeriodicCraft.blockOreUranium, 2)).generate(world, random, Xcoord, Ycoord, Zcoord);
		}
	}
	
	private void generateSurfaceIridium(World world, Random random, int BlockX, int BlockZ) {
		for(int i = 0; i < 2; i++){
			int Xcoord = BlockX + random.nextInt(16);
			int Zcoord = BlockZ + random.nextInt(16);
			int Ycoord = random.nextInt(32);	
			(new WorldGenMinable(PeriodicCraft.blockOreIridium, 2)).generate(world, random, Xcoord, Ycoord, Zcoord);
		}
	}
	
}
