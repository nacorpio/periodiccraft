package com.periodiccraft.pcm.core.knowledge;

import com.periodiccraft.pcm.api.util.ChatUtil;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemKnowledge {

	private String itemName;
	private int progress = 0;
	private boolean knowledgeCompleted = false;
	
	public ItemKnowledge(Item par1) {
		this(par1.getUnlocalizedName());
	}
	
	public ItemKnowledge(ItemStack par1) {
		this(par1.getUnlocalizedName());
	}
	
	public ItemKnowledge(String par1) {
		this(par1, 0);
	}
	
	public ItemKnowledge(String par1, int par2) {
		this.itemName = par1;
		this.progress = par2;
	}

	public final void updateItemStack(ItemStack par1) {
		// TODO: Add an item-lore.
	}
	
	public final String getItemName() {
		return itemName.substring(5);
	}
	
	public final void incrementProgress(int par1) {
		if (this.progress < 100) {
			this.progress += par1;
			this.onProgressChanged(this.progress);
		}
	}
	
	public final void decrementProgress(int par1) {
		if (this.progress > 0) {
			this.progress -= par1;
			this.onProgressChanged(this.progress);
		}
	}
	
	public final void setProgress(int par1) {
		this.progress = par1;
		this.onProgressChanged(this.progress);
	}
	
	public final boolean isCompleted() {
		return this.progress == 100;
	}
	
	public final int getProgress() {
		return progress;
	}
	
	public void onProgressChanged(int par1) {
		if (par1 == 100 || this.isCompleted()) {
			this.onKnowledgeCompleted();	
		}
	}
	
	public void onKnowledgeCompleted() {
		if (!knowledgeCompleted) {
			ChatUtil.sendChatMessage(ChatUtil.StringHandler.green + "Completed the knowledge for item '" + this.getItemName() + "'.");
			this.knowledgeCompleted = true;
		}
	}
	
}
