package com.periodiccraft.pcm.core.knowledge;

import java.util.HashMap;
import java.util.Map;

import com.periodiccraft.pcm.core.element.Part;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public final class KnowledgeBase {

	public static final Map<String, ItemKnowledge> itemKnowledge = new HashMap<String, ItemKnowledge>();
	public static final Map<String, ElementKnowledge> elementKnowledge = new HashMap<String, ElementKnowledge>();
	public static final Map<String, BlockKnowledge> blockKnowledge = new HashMap<String, BlockKnowledge>();
	
	public static final void addKnowledge(String par1, BlockKnowledge par2) {
		if (!hasBlockKnowledge(par1)) {
			blockKnowledge.put(par1, par2);
		}
	}
	
	public static final void addKnowledge(String par1, ItemKnowledge par2) {
		if (!hasItemKnowledge(par1)) {
			itemKnowledge.put(par1, par2);
		}
	}
	
	public static final void addKnowledge(String par1, ElementKnowledge par2) {
		if (!hasElementKnowledge(par1)) {
			elementKnowledge.put(par1, par2);
		}
	}
	
	public static final BlockKnowledge getBlockKnowledge(String par1) {
		return blockKnowledge.get(par1);
	}
	
	public static final BlockKnowledge getBlockKnowledge(Block par1) {
		return blockKnowledge.get(par1.getUnlocalizedName());
	}
	
	public static final ItemKnowledge getItemKnowledge(ItemStack par1) {
		return itemKnowledge.get(par1.getUnlocalizedName());
	}
	
	public static final ItemKnowledge getItemKnowledge(Item par1) {
		return itemKnowledge.get(par1.getUnlocalizedName());
	}
	
	public static final ItemKnowledge getItemKnowledge(String par1) {
		return itemKnowledge.get(par1);
	}
	
	public static final ElementKnowledge getElementKnowledge(Part par1) {
		return elementKnowledge.get(par1.getSymbol());
	}
	
	public static final ElementKnowledge getElementKnowledge(String par1) {
		return elementKnowledge.get(par1);
	}
	
	public static final boolean hasBlockKnowledge(String par1) {
		return blockKnowledge.containsKey(par1);
	}
	
	public static final boolean hasItemKnowledge(String par1) {
		return itemKnowledge.containsKey(par1);
	}
	
	public static final boolean hasElementKnowledge(String par1) {
		return elementKnowledge.containsKey(par1);
	}
	
}
