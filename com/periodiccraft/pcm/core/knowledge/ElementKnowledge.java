package com.periodiccraft.pcm.core.knowledge;

import com.periodiccraft.pcm.core.element.Elements;
import com.periodiccraft.pcm.core.element.Part;

public class ElementKnowledge {

	private String symbol;
	private int progress;
	
	public ElementKnowledge(String par1, int par2) {
		this.symbol = par1;
		this.progress = par2;
	}
	
	public final Part getElement() {
		return Elements.getElement(symbol);
	}
	
	public final String getSymbol() {
		return this.symbol;
	}
	
	public final int getProgress() {
		return this.progress;
	}

	public final void incrementProgress(int par1) {
		if (this.progress < 100) {
			this.progress += par1;
		}
	}
	
	public final void decrementProgress(int par1) {
		if (this.progress > 0) {
			this.progress -= par1;
		}
	}
	
	public final void setProgress(int par1) {
		this.progress = par1;
	}
	
	public final boolean isCompleted() {
		return this.progress == 100;
	}
	
}
