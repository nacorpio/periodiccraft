package com.periodiccraft.pcm.core.knowledge;

import net.minecraft.block.Block;

public class BlockKnowledge {

	private String blockName;
	private int progress;
	
	public BlockKnowledge(Block par1) {
		this(par1.getUnlocalizedName());
	}
	
	public BlockKnowledge(String par1) {
		this(par1, 0);
	}
	
	public BlockKnowledge(String par1, int par2) {
		this.blockName = par1;
		this.progress = par2;
	}
	
	public final String getBlockName() {
		return blockName;
	}
	
	public final int getProgress() {
		return this.progress;
	}

	public final void incrementProgress(int par1) {
		if (this.progress < 100) {
			this.progress += par1;
		}
	}
	
	public final void decrementProgress(int par1) {
		if (this.progress > 0) {
			this.progress -= par1;
		}
	}
	
	public final void setProgress(int par1) {
		this.progress = par1;
	}
	
	public final boolean isCompleted() {
		return this.progress == 100;
	}
	
}
